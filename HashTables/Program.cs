﻿using System;
using System.Collections.Generic;

namespace HashTables
{
    class Program
    {
        static void Main(string[] args)
        {
            HashTable1 hashTable = new HashTable1(10); //Хэш-таблица с разрешением коллизий методом цепочек
            for (int i = 0; i < 10; i++)
            {
                hashTable.Add(i, i * 3);
            }

            Console.WriteLine(hashTable.Search(9));
            hashTable.Remove(5);
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(hashTable.Search(i));
            }
            
            HashTable2 hashTable2 = new HashTable2(10); //Хэш-таблица с разрешением коллизий методом цепочек
            for (int i = 0; i < 10; i++)
            {
                hashTable2.Add(i, i * 3);
            }

            Console.WriteLine(hashTable2.Search(9));
            hashTable2.Remove(5);
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(hashTable2.Search(i));
            }
        }
    }

    public class HashTable2
    {
        private Item[] Items { get; set; }

        public HashTable2(int lenght)
        {
            Items = new Item[lenght];
        }
        
        private int HashFunc(int key, int i)
        {
            return key * Math.Abs(i - 100 * Items.Length / 3 )  % Items.Length;
        }

        public void Add(int key, int value)
        {
            int i = 0;
            int hashFunc = HashFunc(key, i);
            while (Items[hashFunc] != null)
            {
                hashFunc = HashFunc(key, i = i + 1);
            }

            Items[hashFunc] = new Item { Key = key, Value = value};
        }

        public int Search(int key)
        {
            int i = 0;
            int j = HashFunc(key, i);
            while(i < Items.Length)
            {
                i = i + 1;
                j = HashFunc(key, i);
                if (Items[j] != null)
                {
                    if (Items[j].Key == key)
                    {
                        break;
                    }
                }
            }
            if (Items[j] == null)
            {
                return int.MinValue;
            }
            return Items[j].Value;
        }

        public void Remove(int key)
        {
            int i = 0;
            //int hFunc = HashFunc(key, i);
            while(i < Items.Length)
            {
                i = i + 1;
                if (Items[HashFunc(key, i)] != null)
                {
                    if (Items[HashFunc(key, i)].Key == key)
                    {
                        Items[HashFunc(key, i)] = null;
                        break;
                    }
                }
            }
        }
    }

    public class HashTable1 //Хэш-таблица с разрешением коллизий методом цепочек
    {
        private List<Item>[] ArrayList { get; }

        public HashTable1(int lenght)
        {
            ArrayList = new List<Item>[lenght];
        }

        public void Add(int key, int value)
        {
            Item item = new Item {Value = value, Key = key};
            int hashFunc = HashFunc(key);
            if (ArrayList[hashFunc] == null)
            {
                ArrayList[hashFunc] = new List<Item>();
            }

            ArrayList[hashFunc].Add(item);
        }

        private int HashFunc(int key)
        {
            return key % ArrayList.Length;
        }

        public int Search(int key)
        {
            int hFunc = HashFunc(key);
            foreach (var item in ArrayList[hFunc])
            {
                if (item.Key == key)
                {
                    return item.Value;
                }
            }

            return -1;
            //throw new Exception("We don't have this key");
        }

        public void Remove(int key)
        {
            int hFunc = HashFunc(key);
            foreach (var item in ArrayList[hFunc])
            {
                if (item.Key == key)
                {
                    ArrayList[hFunc].Remove(item);
                    break;
                }
            }
        }
    }

    public class Item
    {
        public int Key { get; set; }
        public int Value { get; set; }
    }
}